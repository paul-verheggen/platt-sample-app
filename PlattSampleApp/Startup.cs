﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;
using PlattSampleApp.Controllers;
using PlattSampleApp.Data;
using PlattSampleApp.Infrastructure;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(PlattSampleApp.Startup))]
namespace PlattSampleApp
{
    public partial class Startup
    {        
        public void ConfigureServices(IServiceCollection services)
        {
            // add application services which use DI
            services.AddTransient<IWebApi, WebApi>();
            services.AddTransient<IStarWarsApi, StarWarsApi>();
            services.AddTransient(typeof(HomeController));
        }

        public void Configuration(IAppBuilder app)
        {
            // basic dependency resolver for MVC
            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);
        }
    }
}
