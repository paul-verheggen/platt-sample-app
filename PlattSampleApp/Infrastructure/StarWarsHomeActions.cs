﻿using PlattSampleApp.Data;
using PlattSampleApp.Data.Models;
using PlattSampleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Infrastructure
{
    public class StarWarsHomeActions
    {
        readonly IStarWarsApi Api;

        public StarWarsHomeActions(IStarWarsApi api)
        {
            Api = api;
        }

        public AllPlanetsViewModel GetAllPlanets()
        {
            var allPlanets = Api.GetAllPlanets();

            // convert planets to view model,
            // and order the planets by diameter, largest to smallest
            var orderedPlanets =
                from pViewModel in
                (
                    from p in allPlanets
                    select new PlanetDetailsViewModel()
                    {
                        Name = p.Name,
                        Population = p.Population,
                        Diameter = p.Diameter == "unknown" ? 0 : int.Parse(p.Diameter),
                        Terrain = p.Terrain,
                        LengthOfYear = p.LengthOfYear
                    }
                )
                orderby pViewModel.Diameter descending
                select pViewModel;

            // make sure to average only the planets with a known diameter
            var knownDiameters =
                from p in allPlanets
                where p.Diameter != "unknown"
                select int.Parse(p.Diameter);

            var model = new AllPlanetsViewModel()
            {
                Planets = orderedPlanets.ToList(),
                AverageDiameter = knownDiameters.Average()
            };

            return model;
        }

        public SinglePlanetViewModel GetPlanetDetails(int planetid)
        {
            var p = Api.GetPlanetDetails(planetid);

            var model = new SinglePlanetViewModel()
            {
                Name = p.Name,
                LengthOfDay = p.LengthOfDay,
                LengthOfYear = p.LengthOfYear,
                Diameter = p.Diameter,
                Climate = p.Climate,
                Gravity = p.Gravity,
                SurfaceWaterPercentage = p.SurfaceWaterPercentage,
                Population = p.Population,
            };

            return model;
        }

        public PlanetResidentsViewModel GetPlanetResidents(string planetName)
        {
            var planetDetails = Api.GetPlanetDetailsByName(planetName);

            var residentList = new List<ResidentSummary>();

            // call the API to fetch each resident,
            // convert their details to a view model, and add them to a list.
            foreach (var residentUri in planetDetails.Residents)
            {
                var r = Api.GetPersonDetailsByUri(residentUri);
                residentList.Add(new ResidentSummary()
                {
                    Name = r.Name,
                    Gender = r.Gender,
                    Height = r.Height,
                    Weight = r.Weight,
                    HairColor = r.HairColor,
                    EyeColor = r.EyeColor,
                    SkinColor = r.SkinColor,
                });
            }

            // order residents alphabetically
            var orderedResidents =
                from r in residentList
                orderby r.Name
                select r;

            var model = new PlanetResidentsViewModel()
            {
                Residents = orderedResidents.ToList()
            };

            return model;
        }

        public VehicleSummaryViewModel GetVehicleSummary()
        {
            var vehicleDetails = Api.GetAllVehicles();

            // group vehicles with known cost by manufacturer
            var groupedVehicleStats =
                from v in vehicleDetails
                where v.Cost != "unknown"
                group v by v.ManufacturerName into vg
                select new VehicleStatsViewModel()
                {
                    ManufacturerName = vg.Key,
                    AverageCost = vg.Select(v => double.Parse(v.Cost)).Average(),
                    VehicleCount = vg.Count(),
                };

            // sort by count highest to lowest, then cost highest to lowest
            var orderedVehicleStats =
                from v in groupedVehicleStats
                orderby v.VehicleCount descending, v.AverageCost descending
                select v;

            var model = new VehicleSummaryViewModel()
            {
                Details = orderedVehicleStats.ToList(),
                ManufacturerCount = orderedVehicleStats.Count(),
                // to get the total, we can sum the count of each group
                VehicleCount = orderedVehicleStats.Sum(v => v.VehicleCount),
            };

            return model;
        }

        public PeopleSearchViewModel PeopleSearch(Func<string, string[]> filterValueGetter)
        {
            var people = Api.GetAllPeople();

            // provide the factory all the info needed to extract data from the filters and people list
            var searchFactory = new SearchViewModelFactory<PersonDetails>
            {
                FilterValueGetter = filterValueGetter,
                RecordNameGetter = (r => r.Name)
            };
            searchFactory.AddAttribute("eye_color", "Eye Color", p => p.EyeColor);
            searchFactory.AddAttribute("hair_color", "Hair Color", p => p.HairColor);
            searchFactory.AddAttribute("skin_color", "Skin Color", p => p.SkinColor);

            var model = searchFactory.Create(people);
            return model;
        }
    }
}