﻿using PlattSampleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Infrastructure
{
    /// <summary>
    /// Creates the filters and search results for faceted search.
    /// Search is performed in-memory on a list of records of type T.
    /// </summary>
    public class SearchViewModelFactory<T>
    {
        class SearchAttribute
        {
            public string AttributeKey;
            public string AttributeName;
            public Func<T, string> attributeValueGetter;
        }

        readonly List<SearchAttribute> AttributeDefinitions = new List<SearchAttribute>();

        /// <summary>
        /// Gets the list of filtered values for the given attribute key.
        /// </summary>
        public Func<string, string[]> FilterValueGetter = (k => Array.Empty<string>());

        /// <summary>
        /// Gets the name of each record when displaying search results.
        /// </summary>
        public Func<T, string> RecordNameGetter = (r => "");

        public void AddAttribute(string attributeKey, string attributeName, Func<T, string> attributeValueGetter)
        {
            AttributeDefinitions.Add(new SearchAttribute()
            {
                AttributeKey = attributeKey,
                AttributeName = attributeName,
                attributeValueGetter = attributeValueGetter
            });
        }

        private Dictionary<string, string> ConvertRecordToAttributes(T record)
        {
            var attributes = new Dictionary<string, string>();

            // convert the record into attributes one by one, using the getters provided.
            foreach (var definition in AttributeDefinitions)
            {
                attributes.Add(definition.AttributeKey, definition.attributeValueGetter(record));
            }

            return attributes;
        }

        private bool MatchesFilter(SearchRecordViewModel record, FilterViewModel filter)
        {
            var value = record.Attributes[filter.AttributeKey];
            // check if any of the checked items matches the record's valuea
            return filter.Items.Any(i => i.Checked && i.AttributeValue == value);
        }

        public PeopleSearchViewModel Create(List<T> recordsList)
        {
            // convert the list of records
            var allRecords = recordsList.Select(r => new SearchRecordViewModel()
            {
                Name = RecordNameGetter(r),
                Attributes = ConvertRecordToAttributes(r)
            });

            // create the search filters
            var filters = new List<FilterViewModel>();
            foreach (var definition in AttributeDefinitions)
            {
                // for each attribute, the options should be all of the distinct values
                // we can get this by grouping on attribute value
                var items = 
                    (from r in allRecords
                    group r by r.Attributes[definition.AttributeKey] into gr
                    orderby gr.Key ascending
                    select new FilterItemViewModel()
                    {
                        AttributeValue = gr.Key
                    })
                    .ToList();

                // for each filter that was checked, make sure we check it again.
                // we're regenerating the table so it needs to be added on every request.
                var appliedFilters = FilterValueGetter(definition.AttributeKey);
                foreach (var appliedFilter in appliedFilters)
                {
                    items.First(f => f.AttributeValue == appliedFilter).Checked = true;
                }

                filters.Add(new FilterViewModel()
                {
                    AttributeKey = definition.AttributeKey,
                    AttributeName = definition.AttributeName,
                    Items = items.ToList()
                });
            }

            // select the filters which have at least one item checked,
            // and make sure the results we return have all of those.
            var activeFilters = filters.Where(f => f.Items.Any(i => i.Checked)).ToList();
            var filteredRecords =
                from r in allRecords
                where activeFilters.All(f => MatchesFilter(r, f))
                orderby r.Name ascending
                select r;

            var model = new PeopleSearchViewModel()
            {
                SearchResults = filteredRecords.ToList(),
                Filters = filters
            };

            return model;
        }
    }
}