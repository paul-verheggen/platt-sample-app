﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data.Models
{
    public class ResultSet<T>
    {
        [JsonProperty]
        public string Next;

        [JsonProperty]
        public IEnumerable<T> Results;
    }
}