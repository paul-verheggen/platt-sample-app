﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data.Models
{
    public class PersonDetails
    {
        [JsonProperty]
        public string Name;

        [JsonProperty]
        public string Gender;

        [JsonProperty]
        public string Height;

        [JsonProperty(PropertyName = "mass")]
        public string Weight;

        [JsonProperty(PropertyName = "hair_color")]
        public string HairColor;

        [JsonProperty(PropertyName = "eye_color")]
        public string EyeColor;

        [JsonProperty(PropertyName = "skin_color")]
        public string SkinColor;
    }
}