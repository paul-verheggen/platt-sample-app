﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data.Models
{
    public class VehicleDetails
    {
        [JsonProperty(PropertyName = "cost_in_credits")]
        public string Cost;

        [JsonProperty(PropertyName = "manufacturer")]
        public string ManufacturerName;
    }
}