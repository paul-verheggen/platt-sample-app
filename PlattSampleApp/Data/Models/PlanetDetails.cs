﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data.Models
{
    public class PlanetDetails
    {
        [JsonProperty]
        public string Name;

        [JsonProperty(PropertyName = "rotation_period")]
        public string LengthOfDay;

        [JsonProperty(PropertyName = "orbital_period")]
        public string LengthOfYear;

        [JsonProperty]
        public string Diameter;

        [JsonProperty]
        public string Climate;

        [JsonProperty]
        public string Gravity;

        [JsonProperty]
        public string Terrain;

        [JsonProperty(PropertyName = "surface_water")]
        public string SurfaceWaterPercentage;

        [JsonProperty]
        public string Population;

        [JsonProperty]
        public string[] Residents;
    }
}