﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PlattSampleApp.Data
{
    public class WebApi : IWebApi
    {
        private bool TryGetCache(string uri, out string content)
        {
            //this should probably use DI instead of referencing Cache directly
            content = (string)HttpContext.Current.Cache.Get(uri);
            return content != null;
        }

        private void InsertCache(string uri, string content)
        {
            //this should probably use DI instead of referencing Cache directly
            HttpContext.Current.Cache.Insert(uri, content);
        }

        public string GetResponse(string uri)
        {
            string content;
            if (TryGetCache(uri, out content))
            {
                return content;
            }

            var request = WebRequest.CreateHttp(uri);
            var response = request.GetResponse();

            using (Stream s = response.GetResponseStream())
            using (StreamReader sr = new StreamReader(s))
            {
                content = sr.ReadToEnd();
                InsertCache(uri, content);
                return content;
            }
        }
    }
}