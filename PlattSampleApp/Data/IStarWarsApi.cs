﻿using PlattSampleApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Data
{
    public interface IStarWarsApi
    {
        List<PersonDetails> GetAllPeople();
        PersonDetails GetPersonDetailsByUri(string uri);

        List<PlanetDetails> GetAllPlanets();
        PlanetDetails GetPlanetDetails(int planetid);
        PlanetDetails GetPlanetDetailsByName(string planetName);

        List<VehicleDetails> GetAllVehicles();

    }
}
