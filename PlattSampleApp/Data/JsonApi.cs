﻿using Newtonsoft.Json;
using PlattSampleApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data
{
    public abstract class JsonApi
    {
        IWebApi WebApi;
        protected readonly Uri BaseUri;

        /// <summary>
        /// The maximum number of pages to get from the API for multi-page results.
        /// </summary>
        private static int MaxPages = 20;

        protected JsonApi(IWebApi webApi, Uri baseUri)
        {
            WebApi = webApi;
            BaseUri = baseUri;
        }

        protected string CreateAbsoluteUri(string uri)
        {
            var absoluteUri = new Uri(BaseUri, uri);

            // if we passed in an absolute URI, make sure the new URI doesn't have a different base.
            // this is mainly for handling URIs inside the response, like the "next" property.
            if (!BaseUri.IsBaseOf(absoluteUri))
            {
                throw new ArgumentException($"URI endpoint {absoluteUri} does not match base {BaseUri}");
            }

            return absoluteUri.ToString();
        }
        protected T GetEntity<T>(string uri)
        {
            var absoluteUri = CreateAbsoluteUri(uri);
            var response = WebApi.GetResponse(absoluteUri);
            var responseEntity = JsonConvert.DeserializeObject<T>(response);
            return responseEntity;
        }
         
        protected ResultSet<T> GetResultSet<T>(string uri)
        {
            return GetEntity<ResultSet<T>>(uri);
        }

        protected T GetFirstResult<T>(string uri)
        {
            var responseEntity = GetEntity<ResultSet<T>>(uri);
            return responseEntity.Results.First();
        }

        protected List<T> GetAllResults<T>(string uri)
        {
            var allResults = new List<T>();
            GetAllResults<T>(uri, allResults, page: 1);
            return allResults;
        }

        private void GetAllResults<T>(string uri, List<T> allResults, int page)
        {
            // check the page number to prevent an endless loop, which would cause a stack overflow.
            if (page > MaxPages)
            {
                throw new ArgumentOutOfRangeException($"Results exceeded the maximum of {MaxPages} pages.");
            }

            var resultSet = GetResultSet<T>(uri);

            // add the results from this page
            allResults.AddRange(resultSet.Results);

            // recursively get the next page if there's a next URI
            if (!string.IsNullOrEmpty(resultSet.Next))
            {
                GetAllResults(resultSet.Next, allResults, page + 1);
            }
        }
    }
}