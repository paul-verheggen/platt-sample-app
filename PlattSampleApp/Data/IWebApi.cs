﻿namespace PlattSampleApp.Data
{
    public interface IWebApi
    {
        string GetResponse(string uri);
    }
}