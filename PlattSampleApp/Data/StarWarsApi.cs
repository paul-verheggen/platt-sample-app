﻿using PlattSampleApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Data
{
    public class StarWarsApi : JsonApi, IStarWarsApi
    {
        public StarWarsApi(IWebApi webApi)
            : base(webApi, new Uri("https://swapi.co/api/")) { }

        public List<PersonDetails> GetAllPeople()
        {
            return GetAllResults<PersonDetails>($"people/");
        }

        public PersonDetails GetPersonDetailsByUri(string uri)
        {
            return GetEntity<PersonDetails>(uri);
        }

        public List<PlanetDetails> GetAllPlanets()
        {
            return GetAllResults<PlanetDetails>($"planets/");
        }

        public PlanetDetails GetPlanetDetails(int planetid)
        {
            return GetEntity<PlanetDetails>($"planets/{planetid}/");
        }

        public PlanetDetails GetPlanetDetailsByName(string planetName)
        {
            return GetFirstResult<PlanetDetails>($"planets/?search={planetName}");
        }

        public List<VehicleDetails> GetAllVehicles()
        {
            return GetAllResults<VehicleDetails>($"vehicles/");
        }
    }
}