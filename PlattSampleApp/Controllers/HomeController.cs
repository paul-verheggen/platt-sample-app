﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlattSampleApp.Data;
using PlattSampleApp.Infrastructure;
using PlattSampleApp.Models;

namespace PlattSampleApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly StarWarsHomeActions HomeActions;

        public HomeController(IStarWarsApi api)
        {
            HomeActions = new StarWarsHomeActions(api);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllPlanets()
        {
            var model = HomeActions.GetAllPlanets();
            return View(model);
        }

        public ActionResult GetPlanetTwentyTwo(int planetid)
        {
            var model = HomeActions.GetPlanetDetails(planetid);
            return View(model);
        }

        public ActionResult GetResidentsOfPlanetNaboo(string planetname)
        {
            var model = HomeActions.GetPlanetResidents(planetname);
            return View(model);
        }

        public ActionResult VehicleSummary()
        {
            var model = HomeActions.GetVehicleSummary();
            return View(model);
        }

        [HttpGet]
        public ActionResult PeopleSearch()
        {
            var model = HomeActions.PeopleSearch
            (
                // pass a function for getting filter values from the body
                // always return empty filters since we haven't created any yet
                k => Array.Empty<string>()
            );
            return View(model);
        }

        [HttpPost]
        public ActionResult PeopleSearch(FormCollection formCollection)
        {
            var model = HomeActions.PeopleSearch
            (
                // pass a function for getting filter values from the body
                // if the key doesn't exist, then return an empty string array
                k => formCollection.GetValues(k) ?? Array.Empty<string>()
            );
            return View(model);
        }
    }
}