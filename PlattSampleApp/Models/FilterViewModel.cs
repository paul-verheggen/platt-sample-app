﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class FilterViewModel
    {
        public string AttributeKey;
        public string AttributeName;
        public List<FilterItemViewModel> Items;
    }
}