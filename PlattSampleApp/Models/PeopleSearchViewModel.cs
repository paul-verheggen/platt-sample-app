﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class PeopleSearchViewModel
    {
        public List<FilterViewModel> Filters;
        public List<SearchRecordViewModel> SearchResults;
    }
}