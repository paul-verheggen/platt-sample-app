﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class FilterItemViewModel
    {
        public string AttributeValue;
        public bool Checked;
    }
}