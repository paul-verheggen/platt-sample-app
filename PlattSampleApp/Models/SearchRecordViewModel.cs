﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlattSampleApp.Models
{
    public class SearchRecordViewModel
    {
        public string Name;
        public Dictionary<string, string> Attributes;
    }
}