﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test.Data.Models
{
    public class FooEntity
    {
        [JsonProperty]
        public string Foo;
    }
}
