﻿using PlattSampleApp.Test.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test.Data
{
    public interface ITestApi
    {
        FooEntity GetFooEntity(string uri);
    }
}
