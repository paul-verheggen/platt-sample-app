﻿using PlattSampleApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test.Data
{
    public class MockWebApi : IWebApi
    {
        public string GetResponse(string uri)
        {
            switch(uri)
            {
                case "https://example.com/test/":
                    return @"{""success"": true}";
                case "https://example.com/api/foo/bar/":
                    return @"{""foo"": ""bar""}";
                case "https://example.com/api/foo/baz/":
                    return @"{""foo"": ""baz""}";
                case "https://example.com/api/foos/":
                    return @"{
                        ""next"": ""https://example.com/api/foos/?page=2"",
                        ""results"": [
                            {""foo"": ""bar1""},
                            {""foo"": ""baz1""},
                            {""foo"": ""qux1""}
                        ]
                    }";
                case "https://example.com/api/foos/?page=2":
                    return @"{
                        ""next"": ""https://example.com/api/foos/?page=3"",
                        ""results"": [
                            {""foo"": ""bar2""},
                            {""foo"": ""baz2""},
                            {""foo"": ""qux2""}
                        ]
                    }";
                case "https://example.com/api/foos/?page=3":
                    return @"{
                        ""next"": null,
                        ""results"": [
                            {""foo"": ""bar3""},
                            {""foo"": ""baz3""},
                            {""foo"": ""qux3""}
                        ]
                    }";
                case "https://example.com/api/foos/circular/":
                    // use a circular reference to test an endless set of results
                    return @"{
                        ""next"": ""https://example.com/api/foos/circular/"",
                        ""results"": [
                            {""foo"": ""bar""}
                        ]
                    }";
                default:
                    throw new ArgumentException($"Unexpected URI: {uri}");
            }

        }
    }
}
