﻿using PlattSampleApp.Data;
using PlattSampleApp.Data.Models;
using PlattSampleApp.Test.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test.Data
{
    public class TestApi : JsonApi
    {
        public TestApi(IWebApi api)
            : base(api, new Uri("https://example.com/api/")) { }

        public FooEntity GetFooEntity(string uri)
        {
            return GetEntity<FooEntity>(uri);
        }

        public ResultSet<FooEntity> GetFooResult(string uri)
        {
            return GetResultSet<FooEntity>(uri);
        }

        public List<FooEntity> GetAllFoos(string uri)
        {
            return GetAllResults<FooEntity>(uri);
        }
    }
}
