﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlattSampleApp.Test.Data;
using PlattSampleApp.Test.Data.Models;
using PlattSampleApp.Test.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test
{
    [TestClass]
    public class JsonApiTests
    {
        [TestMethod]
        public void TestRelativeUri()
        {
            var api = DependencyHelper.GetService<TestApi>();
            FooEntity foo;

            foo = api.GetFooEntity("foo/bar/");
            Assert.AreEqual("bar", foo.Foo);
            foo = api.GetFooEntity("foo/baz/");
            Assert.AreEqual("baz", foo.Foo);
        }

        [TestMethod]
        public void TestAbsoluteUri()
        {
            var api = DependencyHelper.GetService<TestApi>();
            FooEntity foo;

            foo = api.GetFooEntity("https://example.com/api/foo/bar/");
            Assert.AreEqual("bar", foo.Foo);
            foo = api.GetFooEntity("https://example.com/api/foo/baz/");
            Assert.AreEqual("baz", foo.Foo);
        }

        [TestMethod]
        public void TestDifferentBaseUri()
        {
            var api = DependencyHelper.GetService<TestApi>();
            Assert.ThrowsException<ArgumentException>(
                () => api.GetFooEntity("https://example.com/test/")
            );
        }

        [TestMethod]
        public void TestResultSet()
        {
            var api = DependencyHelper.GetService<TestApi>();
            var foos = api.GetFooResult("https://example.com/api/foos/");
            var fooArray = foos.Results.ToArray();
            Assert.AreEqual("bar1", fooArray[0].Foo);
            Assert.AreEqual("baz1", fooArray[1].Foo);
            Assert.AreEqual("qux1", fooArray[2].Foo);
        }

        [TestMethod]
        public void TestAllResults()
        {
            var api = DependencyHelper.GetService<TestApi>();
            var foos = api.GetAllFoos("https://example.com/api/foos/");

            Assert.AreEqual("bar1", foos[0].Foo);
            Assert.AreEqual("baz1", foos[1].Foo);
            Assert.AreEqual("qux1", foos[2].Foo);
            Assert.AreEqual("bar2", foos[3].Foo);
            Assert.AreEqual("baz2", foos[4].Foo);
            Assert.AreEqual("qux2", foos[5].Foo);
            Assert.AreEqual("bar3", foos[6].Foo);
            Assert.AreEqual("baz3", foos[7].Foo);
            Assert.AreEqual("qux3", foos[8].Foo);
        }

        [TestMethod]
        public void TestResultsOutOfRange()
        {
            var api = DependencyHelper.GetService<TestApi>();

            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () => api.GetAllFoos("https://example.com/api/foos/circular/")
            );
        }
    }
}
