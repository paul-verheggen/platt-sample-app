﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlattSampleApp.Data;
using PlattSampleApp.Test.Data;
using PlattSampleApp.Test.Infrastructure;

namespace PlattSampleApp.Test
{
    [TestClass]
    public class WebApiTests
    {
        [TestMethod]
        public void TestGetResponse()
        {
            var api = DependencyHelper.GetService<IWebApi>();
            var response = api.GetResponse("https://example.com/test/");
            Assert.AreEqual(@"{""success"": true}", response);
        }
    }
}
