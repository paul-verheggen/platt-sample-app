﻿using Microsoft.Extensions.DependencyInjection;
using PlattSampleApp.Data;
using PlattSampleApp.Test.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlattSampleApp.Test.Infrastructure
{
    public static class DependencyHelper
    {
        static IServiceProvider ServiceProvider;

        static DependencyHelper()
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            ServiceProvider = services.BuildServiceProvider();
        }

        static void ConfigureServices(IServiceCollection services)
        {
            // add application services which use DI
            services.AddTransient<IWebApi, MockWebApi>();
            services.AddTransient(typeof(TestApi));
        }

        public static T GetService<T>()
        {
            return ServiceProvider.GetService<T>();
        }
    }
}
